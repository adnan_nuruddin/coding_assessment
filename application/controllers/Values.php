<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Values extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

	}

	public function getAllValues()
	{
		$keys = $this->input->get('keys');
		$result = [];
		
		if(count($keys)==0)//For getting all datas
		{
			$this->db->select('*');
			$this->db->from('test');
			$data = $this->db->get()->result_array();
		}
		else//For getting selected data
		{
			$keys_array = explode(',',$keys);
			$this->db->select('*');
			$this->db->from('test');
			$this->db->where_in('input_keys',$keys_array);
			$data = $this->db->get()->result_array();
		}


		foreach ($data as $key => $value) {
			$result[$value['input_keys']] = $value['input_values'];
		}

		json_success(200,$result);
	}

	public function addValues()
	{
		$data = $this->input->post();
		$keys = array_keys($data);

		//Checking if data already exists for status
		$this->db->select('*');
		$this->db->from('test');
		$this->db->where_in('input_keys', $keys);
		$result_count = $this->db->get()->num_rows();

		$batch_data = [];
		//Generating array for batch insert
		foreach ($data as $key => $value) {
			array_push($batch_data, array('input_keys'=>$key,'input_values'=>$value));
		}

		//deleting existing keys for faster performance
		$this->db->where_in('input_keys',$keys);
		$this->db->delete('test');

		//Inserting values in batch
		$this->db->insert_batch('test', $batch_data);

		$keys_string = "";
		foreach ($keys as $key => $value) {
			
			if($key == count($keys)-1)
			{
				$keys_string.="'".$value."'";
			}
			else
			{
				$keys_string.="'".$value."'".",";
			}

		}

		//For deleting after 5 minutes
		$query = "CREATE EVENT `my_event".uniqid()."` ON SCHEDULE AT now() + INTERVAL 5 MINUTE 
		ON COMPLETION NOT PRESERVE ENABLE 
		DO  DELETE FROM test where input_keys in(".$keys_string.") ";

		$this->db->query($query);

		if(count($keys)!=$result_count)
		{
			json_success(201);//atleast one data created
		}
		else
		{
			json_success(200);//no new data created. only update operaion performed
		}
	}

}

/* End of file Values.php */
/* Location: ./application/controllers/Values.php */