<?php 

function json_success($status=200,$data=array(),$execution_time="")
{
	$CI =& get_instance();
	if ($execution_time == "") 
	{
		$CI->output->set_content_type('application/json')->set_output(json_encode($data))->set_status_header($status);
	}
	else
	{
		$CI->output->set_content_type('application/json')->set_output(json_encode(array('data'=>$data,'execution_time' => $execution_time)))->set_status_header($status);
	}
}


/* End of file input_output_helper.php */
/* Location: ./application/helpers/input_output_helper.php */