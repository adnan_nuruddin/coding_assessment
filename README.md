# README #

This is the coding assessment of Md. Nuruddin Monsur Adnan held on 20 September, 2017 

### How do I get set up? ###

1. You Will need apache 2, PHP 7.0 and MySQL 5.6+ o run this code
2. Apache mod rewrite must be enabled

### Get Started ###

1. Import the my_code.sql into mysql database
2. Put the folder in the apace www folder

### For Docker ###

1. Extract the Docker.zip folder
2. Inside the folder run this command:
```
	docker build -t my-site2 .
	docker run -d -p 8080:80 my-site2
```

